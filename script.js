fetch('https://jsonplaceholder.typicode.com/todos', { 
	method: 'GET'
}) 
.then((response) => response.json())
.then((data) => {

	let toDoList = data.map(getTitle);

	console.log(toDoList);
})

function getTitle(toDoList) {
  return [toDoList.title];
}

fetch('https://jsonplaceholder.typicode.com/todos/1') // Using .then syntax
.then((response) => response.json())
.then((data) => {
	console.log(data);
	console.log(`The item ${data.title} on the list has a status of ${data.completed}`);	
})

fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: false,
		userId: 1
	})
})
.then ((response) => response.json())
.then((data) => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', { 
	method: 'PUT', 
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		description: 'To update the my to do list with a different data structure',
		status: 'Pending',
		dateCompleted: 'Pending',		
		userId: 1
	})
})
.then ((response) => response.json())
.then((data) => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', { 
	method: 'PATCH', 
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		status: 'Complete',
		dateCompleted: '10/03/2022'		
	})
})
.then ((response) => response.json())
.then((data) => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', { 
	method: 'DELETE'
})